package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.students.list;
import views.html.students.details;
import java.util.List;
import models.Student;
import play.data.Form;
import java.lang.String;
import com.avaje.ebean.Ebean;


public class Students extends Controller{

    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list(){
        List<Student> students = Student.findAll();
        return ok(list.render(students));
    }

    public static Result newAccount(){
        return ok(details.render(studentForm));
    }

    public static Result information(Student student){
        if(student == null){
            return notFound(String.format("Student has MSV %s does not exist.",student.msv));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));
    }

    public static Result save(){
        Form<Student> boundForm = studentForm.bindFromRequest();
        if(boundForm.hasErrors()){
            flash("error","Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Student student = boundForm.get();
		if(student.id == null){
			student.save();
		}else{
			student.update();
		}
        flash("success", String.format("Successfully added student %s",student));
        return redirect(routes.Students.list());
    }

    public static Result delete(String msv){
        final Student student = Student.findByMsv(msv);
        if(student == null){
            return notFound(String.format("Student %s does not exist.",student));
        }
        student.delete();
        return redirect(routes.Students.list());
    }
}