package models;

import java.lang.String;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;
import play.mvc.PathBindable;

@Entity
public class Student extends Model implements PathBindable<Student>
{
	@Id
	public Long id;
	
	@Constraints.Required
	public String msv;
	
    @Constraints.Required
    public String name;

    @Constraints.Required
    public String birthday;

    @Constraints.Required
    public String address;

    @Constraints.Required
    public String phoneNumber;

    @Constraints.Required
    public String sectors;

    @Constraints.Required
    public String courses;

	public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);
    
    public Student(){}
    public Student(String msv,String birthday, String name,String phoneNumber , String address, String sectors, String courses){
		this.msv = msv;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.sectors = sectors;
        this.courses = courses;
        this.birthday = birthday;

        /*SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
        this.birthday = String.valueOf(format.format(birthday));*/
    }

    public String toString(){
        return String.format("%s - %s - %s - %s - %s - %s -%s",msv,phoneNumber, name, birthday, address, sectors, courses);
    }

    public static List<Student> findAll(){
        return find.all();
    }

    public static Student findByMsv(String msv){
        return find.where().eq("msv",msv).findUnique();
	}
    public static List<Student> findByName(String term){
        final List<Student> results = new ArrayList<Student>();
        for(Student candidate : Student.findAll()) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }

	
	@Override
	public Student bind(String key, String value){
		return findByMsv(value);
	}
	
	@Override
	public String unbind(String key){
		return msv;
	}
	
	@Override
	public String javascriptUnbind(){
		return msv;
	}
}